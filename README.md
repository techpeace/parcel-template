# Get a decent start working with Parcel, Gitlab, and Now

The three goals of this project are:

* [x] Work out of the box for GitLab Live Preview, GitLab Pages, and ZEIT Now
* [ ] Promote decent testing and semantic versioning practices
* [ ] Promote good project structure

## Work out of the box for GitLab Live Preview, GitLab Pages, and ZEIT Now

The first goal is achieved through `package.json`, `.gitlab-ci.yml`, `src/index.html`, and `src/index.js`.  By default `.gitlab-ci.yml` will deploy project the `master` branch to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/#gitlab-pages). (Note that it may take up to thirty minutes after repo setup for the website to appear.) All branches can be distributed via tha ZEIT Now serverless framework after [enabling Now for GitLab](https://zeit.co/docs/v2/more/now-for-gitlab) without any modification to `.gitlab-ci.yml`.

GitLab Web IDE has a live preview option which uses [CodeSandbox](https://codesandbox.io/dashboard). The initial configuration of `package.json` enables then vanilla client-side preview environment that emulates parcel and works offline after initial load.  Emulation is not perfect! In this author's experience, the live preview does not currently support [Elm as an Asset Type](https://en.parceljs.org/elm.html).

If you use an Asset Type which cannot be emulated by CodeSandbox client-side, then use the "Open in CodeSandbox" button in the live preview pane to view code changes. `sandbox.config.json` will configure the resulting sandbox to use a node container that actually runs parcel.  (If you are having trouble importing a project into CodeSandbox try opening all desired files in the GitLab Web IDE.)  Note that CodeSandbox does not automatically sync your changes with the GitLab repo.  You will get access to CodeSandbox's collaboritive development features though.

## Promote decent testing and semantic versioning practices

This feature is not implemented yet, but the plan is to configure `.gitlab-ci.yml` to run solutions similar to [semantic-release](https://github.com/semantic-release/semantic-release#readme) for semantic versioning, [jest](https://jestjs.io/) for unit and integration testing, and [cypress](https://www.cypress.io/) for functional testing.  There are other options, like [mocha](https://mochajs.org/) for unit and integration testing, but the previous solutions have desirable properties of being well supported and mostly pre-configured.

One problem with integrating semantic release and GitLab is that [GitLab CE does not support push rules](https://docs.gitlab.com/ee/push_rules/push_rules.html).  This isn't a dealbreaker since one can still fail commits via pipeline, but does make me rethink using GitLab as a primary target for this project.  (Why not target a combination of [stable helm charts](https://hub.helm.sh/charts/stable) which are simpler to deploy on Kubernetes than GitLab currently is?  [GitLab is getting Helm support](https://docs.gitlab.com/charts/), but having a stack agnostic structure could ease template adoption.)

## Promote good project structure

This is inherently a subjective goal, hence why it is the last priority. I am currently doing research into what build systems and repo organizations are used in modern programming.  So far I have determined that monorepo's are desireable because making multiple repo's secure, collaborative, stable, and agile is hard.

One form of monorepo condusive to the spirit of this project would use [`yarn`](https://yarnpkg.com/en/) and [`bit`](https://github.com/teambit/bit).  Both try to be batteries included programs and `yarn` ties to be secure by default.  (For instance, [`yarn` prevents automatic execution of scripts while `npm` did not](https://engineering.fb.com/web/yarn-a-new-package-manager-for-javascript/). [Install scripts are a rising infection vector in `npm`](https://blog.npmjs.org/post/188385634100/npm-security-insights-api-preview-part-2-malware), but, in their defense, [preventing the spread of malware in a public package system is still hard even without install script vulnerabilities](https://blog.npmjs.org/tagged/security))

...

Another 'good project structure' in today's world involves containerizing.  [Docker and Kubernetes are pretty popular in 2019](https://sysdig.com/blog/sysdig-2019-container-usage-report/), and [multi-stage docker builds](https://docs.docker.com/develop/develop-images/multistage-build/) seem like a flexible alternative to `.gitlab-ci.yml` for parcel projects.  I personally have succeeded in running the ARM64 docker community edition in ChromeOS's Linux Beta.  Still have yet to figure out how to get a working parcel / GitLab testing pipeline implemented via a Dockerfile though...
